/************************************************************************************************
 * This program is permutation recognizer inside text file, followed by few rules:
 * 1. Receives 2 arguments from user that contains: file name(text file) and a string.
 * 2. Scan the file for permutations and the word it self.
 * 3. Print all permutations into a text file.
 * 4. Print to command line all permutations found in order it was found in the text file.
 ************************************************************************************************/
#include "permut.h"

int main(int argc, char *argv[]){
	/* File pointer */
	FILE *pFile;
	
	char* word[MAXIMUM_VAL];
	char* fileName;
    file_status status_check = SUCCESS; /* Default initialization for boolean statement */
    fileName       	 =  argv[1];   		/* Filename */
    word[MAXIMUM_VAL]=  argv[2];   		/* Permutation word */
	mode 		 	 =	'r';			/* File mode*/	
	
	/* Check is number of arguments is valid*/
	if(argc < MAXIMUM_ARGUMENTS){
		status_check = VALID_FILE;
		fprintf(stderr,"Sorry, required 2 arguments atleast.\n");
	 }
	 else if (argc > MAXIMUM_ARGUMENTS){
		fprintf(stderr,"Sorry, Too many arguments.");
	 }
	
    /* Cases of various situations of data validation */
    switch(status_check = SUCCESS){      
        
        /* Check if file exist, if not, an error will be printed*/
        case VALID_FILE: fprintf(stderr, "Sorry, filename or location is not valid.\n");
        return EXIT_FAILURE;
        /* Check if file is empty*/
        case EXIST_FILE: fprintf(stderr,"Sorry, the file is empty, please make sure to fill it up.\n");
        return EXIT_FAILURE;
        default: return FAIL;
    }

    /* File open , file name is inserted first as 'argv' argument */
    pFile = fopen(*fileName, mode);
    /* Word will be copied to arguments array to make permutation work */
    strcpy(fileWordsList,*word);
    
    /* First word from file is fetched */
    fscanf(pFile, "%s", fileWordsList);
	
	qsort(fileWordsList, argumentLength, sizeof(char), comparing);
	
    do {
        int i;
        /* Check is length of characters of words that were found in file is matched to permutation word requirement */		
        argumentLength = strlen(fileWordsList);
        difference = argumentLength - strlen(*word);			/* Variable for difference between words value */
        if (difference < 0) { continue; };			/* Check if word is valid by length */
        
		for (i=0; i <= difference; i++)
			if(permutComparing((fileWordsList + i), permutationWord, argumentLength))
			{
				printf("%.*s\n", (int)argumentLength, permutationWord + i);
				counter++;
			}
        printf("%.*s\n", (int)wordsLen,fileWordsList + i);
        counter++;
	} while (fscanf(pFile,"%s", *word) != EOF);
	
	/* Print results */
	if (counter == 0)
		printf("Sorry, No permutations of %s found in %s.\n", *word, fileName);
	else
		printf("%i permutations of %s found in %s file.\n",(int)counter, *word, fileName);

    fclose(pFile);	/* Close file when work is finished */
    return EXIT_SUCCESS;
}
/* Comparing function */
int comparing(const void *x, const void *y)
{
    return (*(char *)x - *(char *)y);
}

/* Permutation comparing function */
int permutComparing(char *word, char *permutationWord, size_t wordsLen){
    char sorted_word[MAXIMUM_VAL]; 							/* Local array for sorting words array */
	strcpy(sorted_word, fileWordsList);						/* Copy of sorted words */
	qsort(sorted_word, wordsLen, sizeof(char), comparing);	/* Sorting of words into sorted words array */
	
	if (memcmp(sorted_word, permutationWord, argumentLength) == 0)
	{
		return (1);											/* word is permutation of arguments */
	}
	return (0);  											/* word is not a permutation of arguments */
}
