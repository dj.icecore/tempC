#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#define MAXIMUM_VAL         512				/* Maximum amount of values */
#define MAXIMUM_ARGUMENTS   3				/* Maximum of arguments could be inserted */

    
/* Comparing function decleration */
int comparing(const void *x, const void *y);
int permutComparing(char* fileWordsList, char* word, size_t wordsLen);
char permutationWord[MAXIMUM_VAL]; 			/* Array of permutation for word that user inserted */
char fileWordsList[MAXIMUM_VAL]; 			/* Words from text file that matches word permutation */
size_t 	argumentLength, wordsLen, counter;
char mode;
int difference;	

/* Checks if file is not empty / exists / arguments counter is filled as required. */
typedef enum {
    EXIST_FILE,   							/* enumeration to check if file exist */
    VALID_FILE,   							/* enumeration to check if file is not empty */
    VALID_ARGC,   							/* enumeration to check if arguments number is valid */
    SUCCESS,
    FAIL
             }file_status;

/* Data type of file status was made for checking file status, part of mentioned file statuses described above */
file_status invalid_file(FILE *pFile),
            empty_file(FILE *pFile),
            valid_argc(int argc);

